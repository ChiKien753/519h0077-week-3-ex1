import 'package:flutter/material.dart';
import 'stream/text_stream.dart';

void main() {
  runApp(const Home());
}

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextStream textStream = TextStream();
  String txt = 'Default';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Text Stream',
      home: Scaffold(
        appBar: AppBar(title: Text('App Bar')),
        body: Text(txt),
        floatingActionButton: FloatingActionButton(
          child:  Text('T'),
          onPressed: (){
            changeText();
          },
        ),
      ),
    );
  }

  changeText() async{
    textStream.getTexts().listen((event) {
      setState(() {
        txt=event;        
      });
    });
  }
}
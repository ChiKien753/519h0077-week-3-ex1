import 'package:flutter/material.dart';
import 'dart:async';

class TextStream{
  StreamController _textController = new StreamController<String>();
  Stream get textStream => _textController.stream;
  final List<String> textList = ['Hey', 'Hi', 'Yo', 'Yes', 'No'];

  Stream<String> getTexts() async* {
  
    yield* Stream.periodic(Duration(seconds: 5), (int t){
      int index = t;
      return textList[index];
    });
  }
  
}